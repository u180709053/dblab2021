use company;
select * from customers;
update customers set country=replace(country, '\n', '');
update customers set city=replace(city, '\n', '');

create view mexicanCustomer as 
select customerid, customername, contactname
from customers 
where country="Mexico";

select * from mexicancustomer;

select * 
from mexicancustomer join orders on mexicancustomer.customerid=orders.customerid;

create view productbelowavg as 
select productid, productname, price
from products
where price<(select avg(price) from products);


delete  from orderdetails where productid=5;
truncate orderdetails;  # faster

delete from customers;
delete from orders; #firstly check relationships

drop table customers;
