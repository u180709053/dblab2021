Select * From proteins;

Explain Select * From proteins Where pid Like "5HT2C_HUMA%";

Create Unique Index idx1 Using Btree On proteins(pid);

Alter Table proteins Drop Index idx1;
Create Unique Index idx1 Using Hash On proteins(pid);

Alter Table proteins Add Constraint acc_pk Primary Key (accession);

Alter Table proteins Drop Primary Key;
Explain Select * From proteins Where accession = "Q9UBA6";